# TodoMVC using Loom

Try it out at: https://loom-lang.gitlab.io/loom-todomvc

## Instalation

Install all dependencies:
```sh
npm install
```

Build the application:
```sh
npm run build
```

Open `index.html`:
```sh
npm run open
```
